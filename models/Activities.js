const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(

    'gestor_actividades',{

        id_actividad:{
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        nombre_actividad:{
            type:Sequelize.STRING,
            allowNull: false
        }

    },
    {
        timestamps: false
    }


)