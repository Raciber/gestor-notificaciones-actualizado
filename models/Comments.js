const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(

    'gestor_comentarios',{

        id_comentario:{
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        //FK
        id_notificacion:{
            type:Sequelize.INTEGER,
            allowNull: false
        },

        date_comentario:{
            type:Sequelize.DATE,
            allowNull: false
        },

        comentario:{
            type:Sequelize.STRING,
            allowNull:false
        },
        
        //FK
        id_users:{
            type: Sequelize.INTEGER,
            allowNull: false
        }

    },
    {
        timestamps: false,
        freezeTableName: true
    }


)