const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(

    'medios_comunica_driver',{

        id_medio:{
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        nombre_medio:{
            type:Sequelize.STRING,
            allowNull: false
        }

    },
    {
        timestamps: false,
        freezeTableName: true,
       

    },



)