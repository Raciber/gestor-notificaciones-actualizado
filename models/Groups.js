const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(

    'grupos_skydash',{


        id_skydash:{
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        name_grupo:{
            type: Sequelize.STRING,
            allowNull: false
        },

        id_grupo:{
            type: Sequelize.INTEGER,
            allowNull: false
        },

        account_telematics:{
            type: Sequelize.STRING,
            allowNull: false
        }



    },
    {
        timestamps: false,
        freezeTableName: true
    }


)