const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(
    'gestor_users',
    {
        id_users: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nombre_users: {
            type: Sequelize.STRING,
            allowNull: false,

        },

        apellido_users: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        
        telematics_users: {
            type: Sequelize.STRING,
            allowNull: false,
        },

        status_users:{
            type:Sequelize.INTEGER,
            allowNull: false,
        },

        type_users: {
            type:Sequelize.INTEGER,
            allowNull: false,
        }
    },
    {
        timestamps: false
    }
    
)
