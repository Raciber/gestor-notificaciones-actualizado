const Sequelize = require("sequelize")
const db = require("../database/db")

module.exports = db.sequelize.define(
    'gestor_notificaciones',
    {
        id_notificacion: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        fecha_notificacion:{
            type: Sequelize.DATEONLY,
            allowNull: false,

        },
        name_notificacion:{
            type: Sequelize.STRING,
            allowNull: false
        },
        unit_notificacion:{
            type: Sequelize.STRING,
            allowNull: false
        },
        google_link:{
            type: Sequelize.STRING,
            allowNull: false
        },
        //Llave foranea
        id_users:{
            type: Sequelize.INTEGER,
            allowNull: true,
        },
        status:{
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: false
        },
        aviso_driver:{
            type: Sequelize.INTEGER,
            allowNull: true
        },
        //Llave foranea
        id_actividad:{
            type: Sequelize.INTEGER,
            allowNull: true
        },
        //Llave foranea
        id_medio:{
            type: Sequelize.INTEGER,
            allowNull: true
        },
        ultima_modificacion_date:{
            type: Sequelize.DATE,
            allowNull: true
        },
        ultima_modificacion_time:{
            type: Sequelize.STRING,
            allowNull: true
        },
        primera_modificacion_date:{
            type: Sequelize.DATE,
            allowNull: true
        },
        primera_modificacion_time:{
            type: Sequelize.STRING,
            allowNull: true
        }
    },
    {
        timestamps: false
    }
    
)
