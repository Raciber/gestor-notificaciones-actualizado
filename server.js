var express = require("express")
var cors = require("cors")
var bodyParser = require("body-parser")
var app = express()
var port = process.env.PORT || 5000

app.use(bodyParser.json())
app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))

var Notifications = require('./routes/Notifications')
var Monitors = require('./routes/Monitores')
var Activities = require('./routes/Activities')
var Comunicaciones = require ('./routes/Comunication')
var Comments = require('./routes/Comments')
var Groups = require ('./routes/Groups')
app.use('/notificaciones', Notifications)
app.use('/monitores', Monitors)
app.use('/activities', Activities)
app.use('/comunication', Comunicaciones)
app.use('/comments', Comments)
app.use('/groups', Groups)
app.listen(port, () =>{
    console.log("Server is running on port: " + port);
})